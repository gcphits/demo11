package com.example.demo11.service;

public interface ConvertJsonService {
    String convertJson(String url, String params);
}