package com.example.demo11.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ThueBaoCoDinhDto {
    private String account;
    private String tenKhachHang;
    private String diaChiLapDat;
    private String maTram;
    private String loaiKhachHang;
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String thuocTinh;
    private String goiCuoc;
    private String hangThueBao;
    private String ngayDauNoi;
    private String dienThoaiLienHe;
}
